package animal;

public class Principal {
    public static void main(String[] args) {
        Animal a = new Animal ("tony");
        System.out.println(a.mover());
        
        System.out.println(" El animal se llama: "+a.getNombre());
        
        a.setcolor("negro");
        System.out.println(" El animal es de color: "+a.getcolor());
        a.setaltura(120);
        System.out.println(" El animal mide: "+a.getaltura() +" cms");
        a.setpeso(120);
        System.out.println(" El animal pesa: "+a.getpeso()+ " lbs");
        
    }
    
}
